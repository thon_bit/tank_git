﻿using UnityEngine;

public class ShellExplosion : MonoBehaviour
{
    public LayerMask m_TankMask;
    public ParticleSystem m_ExplosionParticles;       
    public AudioSource m_ExplosionAudio;              
    public float m_MaxDamage = 100f;                  
    public float m_ExplosionForce = 1000f;            
    public float m_MaxLifeTime = 2f;                  
    public float m_ExplosionRadius = 5f;              


    private void Start()
    {
        Destroy(gameObject, m_MaxLifeTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        // Find all the tanks in an area around the shell and damage them.
		Collider[] colliders = Physics.OverlapSphere(transform.position,m_ExplosionRadius,m_TankMask);

		for (int i = 0; i < colliders.Length; i++) {

			Collider collide = colliders [i];

			Rigidbody rigid = collide.GetComponent<Rigidbody> ();

			if (!rigid)
				continue;

			rigid.AddExplosionForce (m_ExplosionForce, transform.position, m_ExplosionRadius);

			TankHealth health = collide.GetComponent<TankHealth> ();

			if (!health)
				continue;

			float damage = CalculateDamage (rigid.position);

			health.TakeDamage (damage);
		}

		m_ExplosionParticles.transform.parent = null;

		m_ExplosionParticles.Play ();

		m_ExplosionAudio.Play ();

		Destroy (m_ExplosionParticles, m_ExplosionParticles.duration);
		Destroy (gameObject);

    }


    private float CalculateDamage(Vector3 targetPosition)
    {
        // Calculate the amount of damage a target should take based on it's position.
		Vector3 vectorToTarget = transform.position - targetPosition;

		float distance = vectorToTarget.magnitude;

		float relativeDistance = (m_ExplosionRadius - distance) / m_ExplosionRadius;

		float damage = relativeDistance * m_MaxDamage;

		return Mathf.Max (0f, damage);

    }
}